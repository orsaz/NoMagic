package hotel.view;

import hotel.controler.PanelControler;
import hotel.model.rooms.History;
import hotel.controler.WindowControler;
import hotel.model.Rooms;
import java.awt.BorderLayout;
import java.awt.ComponentOrientation;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class RoomsHistoryPage extends javax.swing.JFrame {

    public static History roomsHistory;
    private static Rooms[] rooms;

    public RoomsHistoryPage() {
        initComponents();
    }

    @SuppressWarnings("unchecked")
    private void initComponents() {

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 400, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 300, Short.MAX_VALUE)
        );

        pack();
    }

    public static void main(String args[]) {
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(RoomsHistoryPage.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(RoomsHistoryPage.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(RoomsHistoryPage.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(RoomsHistoryPage.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }

        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new RoomsHistoryPage().setVisible(false);
            }
        });
    }

    public static void createRoomsHistoryPage(Rooms[] roomsData, History history, int roomsNr) {
        roomsHistory = history;
        rooms = roomsData;
        JFrame frame = new JFrame("Rooms history window");
        frame.setSize(600, 400);
        frame.setVisible(true);
        WindowControler windowControler = new WindowControler(frame, rooms);
        windowControler.saveDataOnWindowClosing();

        PanelControler panelClassControler = new PanelControler(rooms);
        JPanel panelForBackButton = new JPanel();
        panelForBackButton.add(panelClassControler.createButtonForGoingBack(frame));
        JPanel isRoomFreePanel = new JPanel();
        JPanel panel = showRoomsHistory();

        if (roomsHistory.getRoomIsTaken()) {
            isRoomFreePanel.add(new JLabel("Room is taken"));
        } else {
            isRoomFreePanel.add(new JLabel("Room is free"));
        }
        frame.add(panelForBackButton, BorderLayout.PAGE_START);
        frame.add(panel, BorderLayout.CENTER);
        frame.add(isRoomFreePanel, BorderLayout.PAGE_END);
    }

    private static JPanel showRoomsHistory() {
        JPanel panel = new JPanel();
        panel.setLayout(new GridLayout(0, 2));
        panel.add(new JLabel("Firstname:"));
        panel.add(new JLabel("Lastname:"));
        
        for (int i = 0; i < roomsHistory.getGuest().size(); i++) {
            panel.add(new JLabel(roomsHistory.getGuest().get(i).getFirstName()));
            panel.add(new JLabel(roomsHistory.getGuest().get(i).getLastName()));
        }
        
        return panel;
    }
}
