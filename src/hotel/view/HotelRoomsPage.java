package hotel.view;

import hotel.controler.PanelControler;
import hotel.controler.WindowControler;
import hotel.model.Rooms;
import static hotel.view.GuestUnregistrationPage.deleteGuest;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class HotelRoomsPage extends javax.swing.JFrame {

    private static Rooms[] rooms;

    public HotelRoomsPage() {
        initComponents();
    }
    @SuppressWarnings("unchecked")
    private void initComponents() {

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 400, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 300, Short.MAX_VALUE)
        );

        pack();
    }
   
    public static void main(String args[]) {
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(HotelRoomsPage.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(HotelRoomsPage.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(HotelRoomsPage.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(HotelRoomsPage.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                Rooms[] rooms = new Rooms[0];
                createGuestNamePage(rooms);
                new HotelRoomsPage().setVisible(true);
            }
        });

    }

    public static void createGuestNamePage(Rooms[] roomsData) {
        rooms = roomsData;
        JFrame frame = new JFrame("Hotel room page");
        frame.setSize(600, 400);
        frame.setVisible(true);
        WindowControler windowControler = new WindowControler(frame, rooms);
        windowControler.saveDataOnWindowClosing();

        PanelControler panelClassControler = new PanelControler(rooms);
        JPanel panelForBackButton = new JPanel();
        panelForBackButton.add(panelClassControler.createButtonForGoingBack(frame));

        JPanel hotelRoomsInfoPanel = panelClassControler.showHotelRoomsInfo(frame);
        frame.add(panelForBackButton, BorderLayout.NORTH);
        frame.add(hotelRoomsInfoPanel, BorderLayout.CENTER);
    }
}
