package hotel.view;

import hotel.controler.PanelControler;
import hotel.model.rooms.History;
import hotel.controler.WindowControler;
import hotel.model.Rooms;
import java.util.ArrayList;
import java.util.Map;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class GuestUnregistrationPage extends javax.swing.JFrame {

    private static Rooms[] rooms;

    public GuestUnregistrationPage() {
        initComponents();
    }

    @SuppressWarnings("unchecked")
    private void initComponents() {

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 400, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 300, Short.MAX_VALUE)
        );

        pack();
    }

    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new GuestUnregistrationPage().setVisible(true);
            }
        });
    }

    public static void createGuestUnregistrationPage(Rooms[] roomsData) {
        rooms = roomsData;

        JFrame frame = new JFrame("GuestPage");
        frame.setSize(600, 400);
        frame.setVisible(true);

        WindowControler windowControler = new WindowControler(frame, rooms);
        windowControler.saveDataOnWindowClosing();

        PanelControler panelClassControler = new PanelControler(rooms);
        JPanel panelForBackButton = new JPanel();
        panelForBackButton.add(panelClassControler.createButtonForGoingBack(frame));

        JPanel panel = showGuestInfo(frame, panelClassControler);
        frame.add(panelForBackButton, BorderLayout.NORTH);
        frame.add(panel, BorderLayout.CENTER);
    }

    public static void deleteGuest(int nr) {
        
        for (int i = 0; i < rooms.length; i++) {
            if (rooms[i].getNumber() - 1 == nr) {
                rooms[i].deleteGuest();
                History[] roomsHistory = MainPage.roomsHistory;
                roomsHistory[i].unregisterGuest();
                createGuestUnregistrationPage(rooms);
                break;
            }
        }
        
    }

    private static JPanel showGuestInfo(JFrame frame, PanelControler panelClassControler) {
        JPanel panel = new JPanel();
        panel.setLayout(new GridLayout(0, 4));
        
        for (int i = 0; i < rooms.length; i++) {
            if (rooms[i].getGuest() != null) {
                panel.add(new JLabel(rooms[i].getGuest().getFirstName()));
                panel.add(new JLabel(rooms[i].getGuest().getLastName()));
                panel.add(new JLabel("Room: " + rooms[i].getNumber()));
                JButton button = panelClassControler.createDeleteButton(frame, i);
                panel.add(button);
            }
        }
        
        return panel;
    }
}
