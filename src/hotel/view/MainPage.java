package hotel.view;

import hotel.controler.PanelControler;
import hotel.model.rooms.History;
import hotel.controler.WindowControler;
import hotel.model.Rooms;
import hotel.model.rooms.Guest;
import java.awt.Component;
import javax.swing.JOptionPane;
import javax.swing.*;
import java.awt.*;
import java.io.*;
import java.nio.file.*;
import java.awt.event.*;

public class MainPage extends javax.swing.JFrame {

    private static int numberOfRoomsInHotel = 5;
    public static String pathForDataFile = "C:\\Users\\nugal\\Desktop\\no magic\\hotel\\src\\hotel\\data.txt";
    private static Rooms[] rooms = readOrCreateRooms(numberOfRoomsInHotel, pathForDataFile);
    public static History[] roomsHistory = createRoomsHistory(numberOfRoomsInHotel);

    public static void main(String args[]) {
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(MainPage.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(MainPage.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(MainPage.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MainPage.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }

        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new MainPage().setVisible(false);
                createMainPage(rooms);
            }
        });
    }

    public static void createMainPage(Rooms[] roomsData) {
        rooms = roomsData;
        JFrame frame = new JFrame("Main page");
        WindowControler windowClassControler = new WindowControler(frame, rooms);
        PanelControler panelClassControler = new PanelControler(rooms);
        windowClassControler.saveDataOnWindowClosing();
        frame.setVisible(true);
        frame.setSize(600, 400);

        JTextField firstNameTextField = new JTextField();
        JTextField lastNameTextField = new JTextField();
        JPanel registrationFormsPanel = panelClassControler.createRegistrationFormsPanel(firstNameTextField, lastNameTextField);
        frame.add(registrationFormsPanel, BorderLayout.PAGE_START);

        JPanel buttonsPanel = new JPanel();
        JButton submitButton = panelClassControler.createRegistrationButton(firstNameTextField, lastNameTextField);
        buttonsPanel.add(submitButton);

        JButton hotelRoomsButton = panelClassControler.createShowHotelRoomsButton(frame);
        buttonsPanel.add(hotelRoomsButton);

        JButton guestRoomPageButton = panelClassControler.createGuestUnregistrationButton(frame);
        buttonsPanel.add(guestRoomPageButton);
        frame.add(buttonsPanel);
    }

    public static boolean registraction(String firstName, String lastName, Rooms[] rooms) {

        if (firstName.isEmpty() || lastName.isEmpty()) {
            JOptionPane.showMessageDialog(null, "Write down your firstname and lastname");
            return false;
        }
        
        for (int i = 0; i < rooms.length; i++) {
            if (rooms[i].getGuest() == null) {
                Guest guest = new Guest(firstName, lastName);
                rooms[i].setGuest(guest);
                roomsHistory[i].addGuestInformation(guest);
                JOptionPane.showMessageDialog(null, "You have booked room: " + (i + 1));
                
                return true;
            }
        }
        
        JOptionPane.showMessageDialog(null, "All rooms are taken");
        
        return false;
    }

    private static Rooms[] readOrCreateRooms(int nr, String path) {
        BufferedReader fileStream = null;
        try {
            fileStream = new BufferedReader(new FileReader(path));
            Rooms[] rooms = new Rooms[nr];
            
            for (int i = 0; i < nr; i++) {
                String line = fileStream.readLine();
                if (line != null && line.length() > 1) {
                    String[] splitedLine = line.split("\\s+");
                    Guest guest = new Guest(splitedLine[1], splitedLine[2]);
                    rooms[i] = new Rooms(guest, Integer.parseInt(splitedLine[0]));
                } else {
                    rooms[i] = new Rooms();
                    rooms[i].setNumber(i + 1);
                }
            }
            
            return rooms;
        } catch (FileNotFoundException fnfe) {
            JOptionPane.showMessageDialog(null, fnfe.getMessage());
        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            try {
                fileStream.close();
            } catch (Exception exp) {
            }
        }
        
        return null;
    }

    private static History[] createRoomsHistory(int howManyRooms) {
        History[] history = new History[howManyRooms];
        for (int i = 0; i < howManyRooms; i++) {
            history[i] = new History();
            history[i].setNumber(i + 1);
        }
        
        return history;
    }
}
