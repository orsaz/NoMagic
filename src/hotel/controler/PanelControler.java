package hotel.controler;

import hotel.view.GuestUnregistrationPage;
import hotel.view.HotelRoomsPage;
import hotel.view.MainPage;
import hotel.view.RoomsHistoryPage;
import hotel.model.rooms.History;
import hotel.model.Rooms;
import static hotel.view.GuestUnregistrationPage.deleteGuest;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

public class PanelControler {

    private Rooms[] rooms;

    public PanelControler(Rooms[] rooms) {
        this.rooms = rooms;
    }

    public JButton createRegistrationButton(JTextField firstNameTextField, JTextField lastNameTextField) {
        JButton button = new JButton("Register");
        button.setFont(new Font("Serif", Font.BOLD, 12));
        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                MainPage mainPage = new MainPage();
                mainPage.registraction(firstNameTextField.getText(), lastNameTextField.getText(), rooms);
            }
        });
        return button;
    }

    public JButton createShowHotelRoomsButton(JFrame frame) {
        JButton button = new JButton("Hotel rooms");
        button.setFont(new Font("Serif", Font.BOLD, 12));
        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                HotelRoomsPage newPage = new HotelRoomsPage();
                frame.dispose();
                newPage.createGuestNamePage(rooms);
            }
        });
        return button;
    }

    public JButton createGuestUnregistrationButton(JFrame frame) {
        JButton button = new JButton("Register out");
        button.setFont(new Font("Serif", Font.BOLD, 12));
        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                GuestUnregistrationPage guestPage = new GuestUnregistrationPage();
                frame.dispose();
                guestPage.createGuestUnregistrationPage(rooms);
            }
        });
        return button;
    }

    public JPanel createRegistrationFormsPanel(JTextField firstNameTextField, JTextField lastNameTextField) {
        JPanel registrationFormsPanel = new JPanel();
        registrationFormsPanel.setLayout(new GridLayout(2, 2));
        registrationFormsPanel.add(new JLabel("Firstname"));
        registrationFormsPanel.add(firstNameTextField);
        registrationFormsPanel.add(new JLabel("Lastname"));
        registrationFormsPanel.add(lastNameTextField);
        return registrationFormsPanel;
    }

    public JButton createDeleteButton(JFrame frame, int nr) {
        JButton button = new JButton("Unregister");
        button.setFont(new Font("Serif", Font.BOLD, 12));
        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                frame.dispose();
                deleteGuest(nr);
            }
        });
        return button;
    }

    public JButton createButtonForGoingBack(JFrame frame) {
        JButton button = new JButton("Back");
        button.setFont(new Font("Serif", Font.BOLD, 12));
        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                frame.dispose();
                MainPage newPage = new MainPage();
                newPage.createMainPage(rooms);
            }
        });
        return button;
    }

    public JButton createRoomHistoryButton(JFrame frame, History roomsHistory, int nr) {
        JButton button = new JButton("show history");
        button.setFont(new Font("Serif", Font.BOLD, 12));
        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                RoomsHistoryPage roomsWindow = new RoomsHistoryPage();
                frame.dispose();
                roomsWindow.createRoomsHistoryPage(rooms, roomsHistory, nr);
            }
        });
        return button;
    }

    public JPanel showHotelRoomsInfo(JFrame frame) {
        JPanel panel = new JPanel();
        panel.setLayout(new GridLayout(0, 4));
        for (int i = 0; i < rooms.length; i++) {
            if (rooms[i].getGuest() != null) {
                panel.add(new JLabel("Room: " + rooms[i].getNumber()));
                panel.add(new JLabel(rooms[i].getGuest().getFirstName()));
                panel.add(new JLabel(rooms[i].getGuest().getLastName()));
                MainPage mainPage = new MainPage();
                JButton button = createRoomHistoryButton(frame, mainPage.roomsHistory[i], i);
                panel.add(button);
            }
            else 
            {
                panel.add(new JLabel("Room: " + rooms[i].getNumber()));
                panel.add(new JLabel(""));
                panel.add(new JLabel(""));
                MainPage mainPage = new MainPage();
                JButton button = createRoomHistoryButton(frame, mainPage.roomsHistory[i], i);
                panel.add(button);
            }
        }
        return panel;
    }
}
