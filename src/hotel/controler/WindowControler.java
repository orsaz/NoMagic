package hotel.controler;

import hotel.model.Rooms;
import static hotel.view.MainPage.pathForDataFile;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import javax.swing.*;

public class WindowControler {

    private JFrame frame;
    private Rooms[] rooms;

    public WindowControler() {
        this.frame = null;
        this.rooms = null;
    }

    public WindowControler(JFrame frame, Rooms[] rooms) {
        this.frame = frame;
        this.rooms = rooms;
    }

    public void saveDataOnWindowClosing() {

        frame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                System.out.println("WindowClosingDemo.windowClosing");
                saveOnClose(pathForDataFile);
                System.exit(0);
            }
        });
    }

    public void saveOnClose(String path) {
        BufferedWriter fileStream = null;
        try {
            Files.deleteIfExists(Paths.get(path));
            fileStream = new BufferedWriter(new FileWriter(path));
            for (int i = 0; i < rooms.length; i++) {
                fileStream.write(Integer.toString(rooms[i].getNumber()));
                if (rooms[i].getGuest() != null) {
                    fileStream.write(" " + rooms[i].getGuest().getFirstName() + " " + rooms[i].getGuest().getLastName());
                }
                fileStream.write("\n");

            }
        } catch (IOException e) {

            e.printStackTrace();

        } finally {
            try {

                if (fileStream != null) {
                    fileStream.close();
                }
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }
}
