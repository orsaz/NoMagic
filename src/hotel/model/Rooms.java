package hotel.model;

import hotel.model.rooms.Guest;

public class Rooms {

    private Guest guest = new Guest();
    private int number = 0;

    public Rooms() {
        this.guest = null;
        this.number = 0;
    }

    public Rooms(Guest guest, int number) {
        this.guest = guest;
        this.number = number;
    }

    public void deleteGuest() {
        setGuest(null);
    }

    public void print() {
        System.out.println(getGuest().getFirstName());
        System.out.println(getNumber());
    }

    public Guest getGuest() {
        return this.guest;
    }

    public void setGuest(Guest guest) {
        this.guest = guest;
    }

    public int getNumber() {
        return this.number;
    }
    
    public void setNumber(int number) {
        this.number = number;
    }
}
