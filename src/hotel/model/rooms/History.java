package hotel.model.rooms;

import hotel.model.rooms.Guest;
import java.util.ArrayList;

public class History {

    private ArrayList<Guest> guest = null;
    private int number = 0;
    private Boolean roomIsTaken;

    public History() {
        guest = new ArrayList<Guest>();
        number = 0;
        roomIsTaken = false;
    }

    public History(ArrayList<Guest> guest, int number) {
        this.guest = guest;
        this.number = number;
        this.roomIsTaken = true;
    }

    public void addGuestInformation(Guest guest) {
        this.getGuest().add(guest);
        this.setRoomIsTaken((Boolean) true);
    }

    public void unregisterGuest() {
        this.setRoomIsTaken((Boolean) false);
    }

    public ArrayList<Guest> getGuest() {
        return this.guest;
    }

    public void setGuest(ArrayList<Guest> guest) {
        this.guest = guest;
    }

    public int getNumber() {
        return this.number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public Boolean getRoomIsTaken() {
        return this.roomIsTaken;
    }

    public void setRoomIsTaken(Boolean roomIsTaken) {
        this.roomIsTaken = roomIsTaken;
    }
}
